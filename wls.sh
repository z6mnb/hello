#!/bin/sh    

source env.properties

export PRE_CLASSPATH="${PRE_CLASSPATH}:$PRO_ROOT/config:$WL_HOME/../modules/javax.persistence_1.1.0.0_2-0.jar:$WL_HOME/../modules/com.oracle.jpa2support_1.0.0.0_2-1.jar"
export EXTRA_JAVA_PROPERTIES="-Dlog4j.configuration=file:///$PRO_ROOT/config/log4j.xml  -Dcom.sun.xml.ws.fault.SOAPFaultBuilder.disableCaptureStackTrace=false"
# export EXTRA_JAVA_PROPERTIES="$EXTRA_JAVA_PROPERTIES -Dhttp.proxyHost=httpproxy -Dhttp.proxyPort=8080 -Dhttp.nonProxyHosts=localhost|127.0.0.1"

export JAVA_OPTIONS="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=5000,server=y,suspend=n"

cp $PRO_ROOT/hello-webapplication/target/hello-webapplication-*-SNAPSHOT.ear $PRO_ROOT/domain/autodeploy

./domain/startWebLogic.sh

