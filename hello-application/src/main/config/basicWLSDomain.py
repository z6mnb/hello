#=======================================================================================
# This is an example of a simple WLST offline configuration script. The script creates
# a simple WebLogic domain using the Basic WebLogic Server Domain template. The script
# demonstrates how to open a domain template, create and edit configuration objects,
# and write the domain configuration information to the specified directory.
#
# This sample uses the demo Pointbase Server that is installed with your product.
# Before starting the Administration Server, you should start the demo Pointbase server
# by issuing one of the following commands:
#
# Windows: WL_HOME\common\eval\pointbase\tools\startPointBase.cmd
# UNIX: WL_HOME/common/eval/pointbase/tools/startPointBase.sh
#
# (WL_HOME refers to the top-level installation directory for WebLogic Server.)
#
# The sample consists of a single server, representing a typical development environment.
# This type of configuration is not recommended for production environments.
#
# Please note that some of the values used in this script are subject to change based on
# your WebLogic installation and the template you are using.
#
# Usage:
#      java weblogic.WLST <WLST_script>
#
# Where:
#      <WLST_script> specifies the full path to the WLST script.
#=======================================================================================

import os

#=======================================================================================
# Open a domain template.
#=======================================================================================

readTemplate(os.environ.get('WL_HOME').replace("\\","/") + "/common/templates/domains/wls.jar")

#=======================================================================================
# Configure the Administration Server and SSL port.
#
# To enable access by both local and remote processes, you should not set the
# listen address for the server instance (that is, it should be left blank or not set).
# In this case, the server instance will determine the address of the machine and
# listen on it.
#=======================================================================================

cd('Servers/AdminServer')
set('Name','server')
set('ListenAddress','')
set('ListenPort', 7002)

#create('server','SSL')
#cd('SSL/server')
#set('Enabled', 'True')
#set('ListenPort', 7002)

#=======================================================================================
# Define the user password for weblogic.
#=======================================================================================

cd('/')
cd('Security/base_domain/User/weblogic')
cmo.setPassword('weblogic123')


#=======================================================================================
# Create and configure a JDBC Data Source, and sets the JDBC user.
#=======================================================================================

cd('/')

create('MyDataSource', 'JDBCSystemResource')
cd('/JDBCSystemResource/MyDataSource')
set('DescriptorFileName','jdbc/MyDataSource-jdbc.xml')
cd('/JDBCSystemResource/MyDataSource/JdbcResource/MyDataSource')
cmo.setName('MyDataSource')

cd('/JDBCSystemResource/MyDataSource/JdbcResource/MyDataSource')
create('myJdbcDataSourceParams','JDBCDataSourceParams')
cd('JDBCDataSourceParams/NO_NAME_0')
set('JNDIName', java.lang.String("MyDataSource")) 
set('GlobalTransactionsProtocol', 'OnePhaseCommit')

cd('/JDBCSystemResource/MyDataSource/JdbcResource/MyDataSource')
create('myeJdbcDriverParams','JDBCDriverParams')
cd('JDBCDriverParams/NO_NAME_0')
set('DriverName','oracle.jdbc.OracleDriver')
set('URL','jdbc:oracle:thin:@localhost:1521:xe')

set('PasswordEncrypted', 'test123')

create('myProps','Properties')
cd('Properties/NO_NAME_0')
create('user', 'Property')
cd('Property/user')
cmo.setValue('myuser')


cd('/JDBCSystemResource/MyDataSource/JdbcResource/MyDataSource')
create('MyDataSourceJdbcConnectionPoolParams','JDBCConnectionPoolParams')
cd('JDBCConnectionPoolParams/NO_NAME_0')
set('CapacityIncrement',1)
set('InitialCapacity',5)
set('MaxCapacity',25)
set('TestTableName','SQL SELECT 1 FROM DUAL')


#=======================================================================================
# Target resources to the servers.
#=======================================================================================

cd('/')
assign('JDBCSystemResource', 'MyDataSource', 'Target', 'server')



#=======================================================================================
# Write the domain and close the domain template.
#=======================================================================================

setOption('OverwriteDomain', 'true')
writeDomain(os.environ.get('PRO_ROOT').replace("\\","/") + "/domain")
closeTemplate()

#=======================================================================================
# Exit WLST.
#=======================================================================================

exit()
