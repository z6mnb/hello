create database ora10
  logfile   group 1 ('D:\oracle\databases\ora10\redo1.log') size 10M,
            group 2 ('D:\oracle\databases\ora10\redo2.log') size 10M,
            group 3 ('D:\oracle\databases\ora10\redo3.log') size 10M
  character set          WE8ISO8859P1
  national character set utf8
  datafile 'D:\oracle\databases\ora10\system.dbf' 
            size 50M
            autoextend on 
            next 10M maxsize unlimited
            extent management local
  sysaux datafile 'D:\oracle\databases\ora10\sysaux.dbf' 
            size 10M
            autoextend on 
            next 10M 
            maxsize unlimited
  undo tablespace undo
            datafile 'D:\oracle\databases\ora10\undo.dbf'
            size 10M
  default temporary tablespace temp
            tempfile 'D:\oracle\databases\ora10\temp.dbf'
            size 10M;
