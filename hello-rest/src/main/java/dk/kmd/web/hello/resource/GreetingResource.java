/**
 *
 */
package dk.kmd.web.hello.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dk.kmd.web.hello.dao.PersonDao;
import dk.kmd.web.hello.model.Person;

@Service
@Path("/greeting")
public class GreetingResource {

	@Autowired
	private PersonDao greetingDao;

	@Transactional(readOnly = true)
	@GET
	@Path("/{param}")
	public Response getGreeting(@PathParam("param") String cpr) {

		Person person = greetingDao.findPersonByCpr(cpr);
		String output = person!=null ? "KMD say : Hello " + person.getNavn():"Person not found try again ;)";

		return Response.status(200).entity(output).build();

	}

}
