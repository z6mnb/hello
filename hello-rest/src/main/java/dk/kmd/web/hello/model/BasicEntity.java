/**
 * 
 */
package dk.kmd.web.hello.model;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.util.StringUtils;

/**
 * @author mnb@kmd.dk
 *
 */
@MappedSuperclass
@EntityListeners(dk.kmd.web.hello.listener.AuditEventlistener.class)
public abstract class BasicEntity {

	@Id
	@Column(name = "ID", length = 22, nullable = false)	
	private String id;

	@Column(name = "OPDATERING_INIT", length = 4, nullable = false)
	private String init;

	@Column(name = "OPDATER_VARKTOJ_ID", length = 8, nullable = false)
	private String app;

	@Column(name = "OPDATERING_TS", nullable = false)
	private Timestamp timestamp;

	public BasicEntity() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public boolean isNew() {
		return (!StringUtils.hasText(id));
	}

	@Override
	public boolean equals(Object o) {
		boolean result;

		if (this == o) {
			return true;
		}
		if (!(o instanceof BasicEntity)) {
			return false;
		}
		String id1 = getId();
		String id2 = ((BasicEntity) o).getId();
		result = id1.equals(id2);

		return result;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}