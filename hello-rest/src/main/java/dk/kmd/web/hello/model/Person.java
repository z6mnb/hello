package dk.kmd.web.hello.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "HELLO_PERSON")
public class Person extends BasicEntity {
	
    @Column(name = "CPR", length = 10, nullable = false)
	private String cpr;
    
    @Column(name = "NAVN", length = 40, nullable = false)
    private String navn;
	
    @Column(name = "ALDER", length = 40, nullable = false)
    private Integer alder;
    public Person() {
		super();		
	}
	public Person(String cpr, String navn, Integer alder) {
		super();
		this.cpr = cpr;
		this.navn = navn;
		this.alder = alder;
	}
	public String getCpr() {
		return cpr;
	}
	public String getNavn() {
		return navn;
	}
	public Integer getAlder() {
		return alder;
	}	
}
