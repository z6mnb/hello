package dk.kmd.web.hello.listener;

import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import dk.kmd.web.hello.model.BasicEntity;

public class AuditEventlistener {
    private static final String UPDATE_INIT = "test";
    private static final String UPDATE_VAEKTOJ = "service";
    											  					
    @PrePersist
    public void onCreate(Object entity) {
        if (entity instanceof BasicEntity) {
            updateRevision((BasicEntity) entity);

        }
    }

    @PreUpdate
    public void onPersist(Object entity) {
        if (entity instanceof BasicEntity) {
            updateRevision((BasicEntity) entity);

        }
    }

    private void updateRevision(BasicEntity entity) {

        entity.setApp(UPDATE_VAEKTOJ);
        entity.setInit(UPDATE_INIT);
        entity.setTimestamp(new Timestamp(new Date().getTime()));

    }

}
