/**
 * 
 */
package dk.kmd.web.hello.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import dk.kmd.web.hello.dao.PersonDao;
import dk.kmd.web.hello.model.Person;

/**
 * @author mnb@kmd.dk
 *
 */
@Repository
public class PersonDaoImpl extends GenericDaoImpl<Person, String> implements PersonDao {

	public Person findPersonById(String id) {
		return find(id);
	}

	public Person findPersonByCpr(String cpr) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
		Root<Person> person = criteriaQuery.from(Person.class);
		criteriaQuery.where(criteriaBuilder.equal(person.get("cpr"), criteriaBuilder.parameter(String.class, "cpr")));
		Query query = em.createQuery(criteriaQuery);
		query.setParameter("cpr", cpr);

		Person foundPerson = null;
		try {
			foundPerson = (Person) query.getSingleResult();
		} catch (NoResultException nre) {
		}
		return foundPerson;
	}

	@SuppressWarnings("unchecked")
	public List<Person> findPersonByAlder(Integer alder) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
		Root<Person> person = criteriaQuery.from(Person.class);
		criteriaQuery.where(criteriaBuilder.equal(person.get("alder"), alder));
		Query query = em.createQuery(criteriaQuery);
		return query.getResultList();
	}

}
