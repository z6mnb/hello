package dk.kmd.web.hello.dao;

import java.io.Serializable;
import java.util.Map;

/**
 * @author mnb (Morten Bertelsen)
 *
 */
public interface GenericDao<T, PK extends Serializable> {

	long countAll(final Map<String, Object> params);

	T create(final T t);

	void delete(final PK primaryKey);

	T find(final PK id);

	T update(final T t);

}
