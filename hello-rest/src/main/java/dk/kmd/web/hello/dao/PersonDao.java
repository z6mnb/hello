package dk.kmd.web.hello.dao;

import java.util.List;

import dk.kmd.web.hello.model.Person;

public interface PersonDao {
	
	Person findPersonById(String id);
	Person findPersonByCpr(String cpr);
	List<Person> findPersonByAlder(Integer alder);
	
}
